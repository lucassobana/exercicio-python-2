class BankAccount:
    def __init__(self, numero_conta, nome, saldo=0):
        self.numero_conta = numero_conta
        self.nome = nome
        self.saldo = saldo

    def alterar_nome(self, nome):
        self.nome = nome
        print("Nome alterado com sucesso.")

    def sacar(self, valor):
        if valor > 0 and valor <= self.saldo:
            self.saldo -= valor
            print(f"Saque de R${valor:.2f} realizado com sucesso.")
        else:
            print("Saldo insuficiente para realizar o saque.")

    def depositar(self, valor):
        if valor > 0:
            self.saldo += valor
            print(f"Depósito de R${valor:.2f} realizado com sucesso.")
        else:
            print("Valor de depósito inválido.")

def menu_conta(conta):
    while True:
        print("\n-----Bem-vindo ao Banco----")
        print("1. Consultar saldo")
        print("2. Alterar nome do titular da conta")
        print("3. Realizar saque")
        print("4. Realizar depósito")
        print("5. Sair")

        opcao = int(input("Escolha uma opção: "))

        if opcao == 1:
            print(f"Saldo atual: R${conta.saldo:.2f}")
        
        elif opcao == 2:
            novo_nome = input("Digite o novo nome do titular da conta: ")
            conta.alterar_nome(novo_nome)

        elif opcao == 3:
            valor_saque = float(input("Digite o valor a ser sacado: "))
            conta.sacar(valor_saque)

        elif opcao == 4:
            valor_deposito = float(input("Digite o valor a ser depositado: "))
            conta.depositar(valor_deposito)
        
        elif opcao == 5:
            print("Obrigado por utilizar nossos serviços. Até mais!")
            break
        
        else:
            print("Opção inválida. Tente novamente.")

numero_conta = input("Digite o número da conta: ")
nome_titular = input("Digite o nome do titular da conta: ")
saldo_inicial = float(input("Digite o saldo inicial da conta:"))

conta = BankAccount(numero_conta, nome_titular, saldo_inicial)
print("\nConta bancária criada com sucesso.")

menu_conta(conta) 

