# class Aluno(object):
    
#     def __init__(self,nome,idade):
#         self.nome = nome
#         self.idade = idade
#         self.nota_1 = 0
#         self.nota_2 = 0
    
#     def addNotas(self,nota_1,nota_2):
#         self.nota_1 = nota_1
#         self.nota_2 = nota_2
        
#     def media(self):
#         return (self.nota_1 + self.nota_2)/2
        
        
# aluno_1 = Aluno("João", 20)
# print(aluno_1.nome, aluno_1.idade)

# aluno_2 = Aluno("Maria", 22)
# aluno_2.addNotas(5, 6)

# calc_media = Aluno.media(aluno_2)

# print(calc_media)

# if calc_media >= 6:
#     print(f'Aprovado com média {calc_media}')
# elif calc_media < 6 or calc_media > 4:
#     print(f'Em recuperação com média {calc_media}')
# else:
#     print(f'Reprovado com média {calc_media}')

# class Animal(object):
#     pass

# class Ave(Animal):
#     pass

# class BeijaFlor(Ave):
#     pass

# class Filha(object):
#     nome = 'Maria'
#     olhos = 'castanhos'
    
# class Pai(object):
#     nome = 'João'
#     sobrenome = 'Silva'
#     residencia = 'Santa Catarina'
#     olhos = 'azuis'

# class Filha(Pai):
#     nome = 'Maria'
#     olhos = 'castanhos'

# class Neta(Filha):
#     nome = 'Ana'
    
# print(Pai.nome,Filha.nome,Neta.nome)
# print(Pai.olhos,Filha.olhos,Neta.olhos)
# print(Pai.sobrenome,Filha.sobrenome,Neta.sobrenome)
# print(Pai.residencia,Filha.residencia,Neta.residencia)

# class Pessoa(object):
#     nome = None
#     idade = None
    
#     def __init__(self,nome,idade):
#         self.nome = nome
#         self.idade = idade
        
#     def envelhecer(self):
#         self.idade += 1
    
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
    
#     def __init__(self,nome,idade,peso):
#         super().__init__(nome,idade)
#         self.peso = peso
    
#     def aquecer(self):      
#         print(f'{self.nome} aquecendo')
        
#     def aposentar(self):
#         self.aposentado = True
        
# class Corredor(Atleta):        
#     def correr(self):
#         print(f'{self.nome} correndo')
        
# class Nadador(Atleta):
#     def nadar(self):
#         print(f'{self.nome} correndo')
        
# class Ciclista(Atleta):
#     def pedalar(self):
#         print(f'{self.nome} correndo')
        
# corredor_1 = Corredor('João', 30, 80)
# nadador_1 = Nadador('Maria', 25, 60)
# ciclista_1 = Ciclista('José', 35, 90)

# corredor_1.aquecer()
# nadador_1.nadar()
# ciclista_1.pedalar()

# def decorador(func):
#     def wrapper():
#         print('Antes da função')
#         func()
#         print('Depois da função')
#     return wrapper

# @decorador
# def media_da_turma():
#     nota_1 = 10
#     nota_2 = 8
#     print(f'Media = {(nota_1 + nota_2)/2}')

# media_da_turma()

class CalculoImposto():
    def __init__(self):
        self._taxa = 0
        
    @property
    def taxa(self):
        return round(self._taxa, 2)
    
    @taxa.setter
    def taxa(self, valor):
        if valor < 0:
            self._taxa = 0
        elif valor > 100:
            self._taxa = 100
        else:
            self._taxa = valor
            
cal = CalculoImposto()
cal.taxa = 55.25
print(cal.taxa)

cal.taxa = 105.60

print(cal.taxa)
