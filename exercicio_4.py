class ControlarDesconto():
    def __init__(self):
        self.preco_produto = 0
        self._porcentagem_desconto = 0
        
    @property
    def porcentagem_desconto(self):
        return self._porcentagem_desconto
    
    @porcentagem_desconto.setter
    def porcentagem(self, valor):
        if valor < 1 or valor > 35:
            self._porcentagem_desconto = 1
        else:
            self._porcentagem_desconto = int(valor)
            
    def aplicar_desconto(self):
        desconto = (self._porcentagem_desconto / 100) * self.preco_produto
        preco_com_desconto = self.preco_produto - desconto
        print(f"Desconto de {self.porcentagem_desconto}% aplicado.")
        print(f"Preço com desconto: R${preco_com_desconto:.2f}")
            
        
controle_desconto = ControlarDesconto()

preco = float(input("Digite o preço do produto: R$"))
controle_desconto.preco_produto = preco

porcentagem = float(input("Digite a porcentagem de desconto desejada (entre 1 e 35): "))
controle_desconto.porcentagem = porcentagem

controle_desconto.aplicar_desconto()
