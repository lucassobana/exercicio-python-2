class Veiculo():
    def __init__(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco
        
    def __str__(self):
        return f"{self.marca} {self.modelo} - R${self.preco:.2f}"

class LojaVeiculos():
    def __init__(self):
        self.car = []
    
    def cadastro(self, marca, modelo, preco):
        
        carro = Veiculo(marca, modelo, preco)     
        self.car.append(carro)   
        
        print(f"Veículo da marca {marca} e modelo {modelo} cadastrado com sucesso!")
        
    def listar(self):
        if not self.car:
            print("Não há veículos cadastrados no estoque.")
        else:
            print("Veículos disponíveis para venda:")
            for idx, carro in enumerate(self.car, start=1):
                print(f"{idx}. {carro}")

        
    def compras(self,indice):
        if 1 <= indice <= len(self.car):
            veiculo_comprado = self.car.pop(indice - 1)
            print(f"Você comprou o veículo {veiculo_comprado.marca} {veiculo_comprado.modelo}.")
        else:
            print("Índice inválido. Tente novamente.")
        
veiculo = LojaVeiculos()

while True:
    
    print("\n-----Bem-vindo à Loja de Veículos----")
    print("1. Cadastrar novo veículo")
    print("2. Listar veículos disponíveis")
    print("3. Comprar veículo")
    print("4. Sair")

    opcao = int(input("Escolha uma opção: "))

    if opcao == 1:
        marca = input("Digite a marca do veículo: ")
        modelo = input("Digite o modelo do veículo: ")
        preco = float(input("Digite o preço do veículo: "))
        veiculo.cadastro(marca, modelo, preco)

    elif opcao == 2:
        veiculo.listar()

    elif opcao == 3:
        veiculo.listar()
        if veiculo.car:
                indice = int(input("Digite o número do veículo que deseja comprar: "))
                veiculo.compras(indice) 
    elif opcao == 4:
        print("Saindo...!")
        break
    else:
        print("Opção inválida.")