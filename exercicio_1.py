class Man:
    def __init__(self, nome):
        self.nome = nome
        self.sexo = 'masculino'
    
    def presentation(self):
        print(f"Olá, sou o {self.nome} do sexo masculino.")

class Woman:
    def __init__(self, nome):
        self.nome = nome
        self.sexo = 'feminino'
    
    def presentation(self):
        print(f"Olá, sou a {self.nome}do sexo feminino.")

def create_person():
    sexo = int(input("Digite 1 se você é homem ou 2 se você é mulher: \n"))
    nome = input("Digite seu nome: ")

    if sexo == 1:
        pessoa = Man(nome)
    elif sexo == 2:
        pessoa = Woman(nome)
    else:
        print("Opção inválida. Tente novamente.")
        return None
    
    return pessoa

pessoa = create_person()
if pessoa:
    pessoa.presentation()

