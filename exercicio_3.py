class Pessoa(object):
    nome = None
    sexo = None
    idade = None
    
    def __init__(self,nome,idade,sexo):
        self.nome = nome
        self.idade = idade
        self.sexo = sexo
        
class Cidadao(Pessoa):
    cpf = None
    
    def __init__(self,nome,idade,sexo,cpf):
        super().__init__(nome,idade,sexo)
        self.cpf = cpf
        
    def dados(self):
        print('-------------------')
        print('Dados da pessoa:')
        print(f'Nome: {self.nome}\nIdade: {self.idade}\nSexo: {self.sexo}\nCPF: {self.cpf}')
        
print('Entre com os dados de uma pessoa:')
nome = input('Nome: ')
idade = int(input('Idade: '))
sexo = input('Sexo: ')
cpf = input('CPF: ')
pessoa_1 = Cidadao(nome, idade, sexo, cpf)
pessoa_1.dados()